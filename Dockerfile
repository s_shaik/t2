# syntax = docker/dockerfile:experimental
FROM node:11.13.0-alpine

# create destination directory
RUN mkdir -p /usr/src/nuxt-app
WORKDIR /usr/src/nuxt-app

# update and install dependency
RUN apk update && apk upgrade
RUN apk add git

RUN printenv


# start the app
CMD [ "npm", "version" ]
